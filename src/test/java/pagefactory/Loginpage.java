package pagefactory;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
public class Loginpage {

	@FindBy(id="userName")
	public static WebElement userid;

	@FindBy(id="password")
	public static WebElement pwd;

	@FindBy(id="submit")
	public static WebElement login;

//	@FindBy(how = How.XPATH, using = "//a[@class='ng-trigger ng-trigger-animation ng-tns-c35-2 session p-dialog p-dynamic-dialog p-component ng-star-inserted']" )
//	public static WebElement session;
	
	@FindBy(xpath= "/html/body/p-dynamicdialog/div/div/div[2]")
	public static WebElement session;
	
	
	@FindBy(xpath="/html/body/p-dynamicdialog/div/div/div[2]/app-confirmation-popup/div/button[2]")
	public static WebElement sessionyes;

	@FindBy(xpath="/html/body/p-dynamicdialog/div/div/div[2]/app-confirmation-popup/div/button[1]")
	public static WebElement sessionno;

	@FindBy(xpath="//div[contains(@class,'ng-trigger ng-trigger-animation ng-tns-c35-3 startofday p-dialog p-dynamic-dialog p-component ng-star-inserted')]")
	public static WebElement startdaypopup;

	@FindBy(xpath="/html/body/p-dynamicdialog/div/div/div[2]/app-lookup/div/app-start-day/div[1]/div[2]/app-input-key/div/div[5]/div[1]/span")
	public static WebElement enteramount;

		
}
