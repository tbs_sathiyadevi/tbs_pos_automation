package utilities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.BeforeSuite;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Browsersetup extends SetUp {
	
	public static WebDriver StartBrowser() {
		try {
			 
			// new SetUp().setProperties();
			String browsername = prop.getProperty("browser");
			// If the browser is Firefox
			if (browsername.equalsIgnoreCase("Firefox")) {
				// Set the path for geckodriver.exe
				WebDriverManager.firefoxdriver().setup();
				driver = new FirefoxDriver();
			}

			// If the browser is Chrome
			else if (browsername.equalsIgnoreCase("Chrome")) {
				// Set the path for chromedriver.exe
				WebDriverManager.chromedriver().setup();
				driver = new ChromeDriver();
			}
			// If the browser is IE
			else if (browsername.equalsIgnoreCase("IE")) {
				// Set the path for IEdriver.exe
				WebDriverManager.iedriver().setup();
				driver = new InternetExplorerDriver();
			} else if (browsername.equalsIgnoreCase("Edge")) {
				// Set the path for edgedriver.exe
				WebDriverManager.edgedriver().setup();
				driver = new EdgeDriver();
			} else if (browsername.equalsIgnoreCase("Safari")) {
				// Set the path for safaridriver.exe
				WebDriverManager.safaridriver().setup();
				driver = new SafariDriver();
			}
			driver.manage().window().maximize();
			
		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
			e.printStackTrace();
		}
		return driver;
		
	}
}
