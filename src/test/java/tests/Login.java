package tests;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;

import pagefactory.Loginpage;
import utilities.Browsersetup;
import utilities.SetUp;

public class Login extends Browsersetup {

	@Test(priority = 0)
	public void setup() {
		setProperties();
		Browsersetup b = new Browsersetup();
		driver = b.StartBrowser();
		driver.get(prop.getProperty("url"));
		ExtentSparkReporter htmlReporter = new ExtentSparkReporter(
				new File(System.getProperty("user.dir") + "/Reports/POS.html"));
		// create ExtentReports and attach reporter(s)
		ExtentReports extent = new ExtentReports();
		extent.attachReporter(htmlReporter);
		ExtentTest test = extent.createTest("POS","Reports");
	}

	@Test(priority = 1)
	public void imagecheck() {

		WebElement imageverify = driver
				.findElement(By.xpath("/html/body/app-root/app-login/div/div/div[1]/form/div[1]/img"));
		Boolean img = (Boolean) ((JavascriptExecutor) driver).executeScript("return arguments[0].complete "
				+ "&& typeof arguments[0].naturalWidth != \"undefined\" " + "&& arguments[0].naturalWidth > 0",
				imageverify);
		if (img) {
			System.out.println("Al-Shamil logo present in login screen");
		} else {
			System.out.println("Al-Shamil logo is not present in login screen");
		}
	}

	@Test(priority = 2)
	public void languagecheck() {

		WebElement globalsymbol = driver
				.findElement(By.xpath("/html/body/app-root/app-login/div/div/div[1]/form/div[2]/div[4]/a/img"));
		Boolean img = (Boolean) ((JavascriptExecutor) driver).executeScript("return arguments[0].complete "
				+ "&& typeof arguments[0].naturalWidth != \"undefined\" " + "&& arguments[0].naturalWidth > 0",
				globalsymbol);
		if (img) {
			System.out.println("Language change option is present in login screen");
		} else {
			System.out.println("Language change option is not present in login screen");
		}
	}

	@Test(priority = 3)
	public void validlogin() throws IOException {
		Integer userCount = jdbcTemplate.queryForObject("select count(*) from POS_USER where USER_NAME=?;",
				new Object[] { prop.getProperty("username") }, Integer.class);
		System.out.println("The user exist in the POS database: " + userCount);
		PageFactory.initElements(driver, Loginpage.class);
		Loginpage.userid.sendKeys(prop.getProperty("username"));
		Loginpage.pwd.sendKeys(prop.getProperty("password"));
		takeScreenShot("LoginPage");
		Loginpage.login.click();
		extendReport("Login", "Login successful");
		

	}

	@Test(priority = 4)
	public void validloginsession() throws IOException {

		PageFactory.initElements(driver, Loginpage.class);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		if (Loginpage.session.isEnabled()) {
			System.out.println("Login Session pop up present");
			System.out.println(prop.getProperty("loginsession"));
			if (prop.getProperty("loginsession").equalsIgnoreCase("no")) {
				Loginpage.sessionno.click();
				System.out.println("Start of the day pop up present");
			}

			else {
				Loginpage.sessionyes.click();
				System.out.println("Existing Session was open");
			}
		}

		else {
			System.out
					.println("Login session pop up not present because no existing session available for logged user");

		}
	}

}