package tests;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import pagefactory.Hamburgersidepage;
import utilities.Browsersetup;
import utilities.SetUp;

public class SideMenu extends Browsersetup {

	@Test(priority = 5)
	public void Headerverify() throws IOException {

		PageFactory.initElements(driver, Hamburgersidepage.class);
		String expectedHeader = "Shamil Pos";
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		String header = Hamburgersidepage.Headername.getText();
		if (expectedHeader.equalsIgnoreCase(header))
			System.out.println("The expected heading is same as actual heading --- " + header);
		else
			System.out.println("The expected heading doesn't match the actual heading --- " + header);

	}

	@Test(priority = 6)
	public void Sidemenuaction() throws IOException {
		PageFactory.initElements(driver, Hamburgersidepage.class);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
		jsExecutor.executeScript("arguments[0].style.background='yellow'", Hamburgersidepage.Sidemenubar);
		System.out.println(Hamburgersidepage.Sidemenubar.isEnabled());
		Hamburgersidepage.Sidemenubar.click();
		takeScreenShot("HomePage");
	}

	@Test(priority = 7)
	public void Favorite() throws IOException {
		for (WebElement element : Hamburgersidepage.listelements) {

			if (element.getText().equals("Favorite")) {
				element.click();
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				driver.findElement(By.xpath("/html/body/p-dynamicdialog/div/div")).isDisplayed();
				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
				String ExpectedHeader = "Favorite Items";
				Assert.assertEquals(ExpectedHeader, Hamburgersidepage.Tableheadername.getText());
				Hamburgersidepage.Closebutton.click();
				break;
			}
		}
	}

	@Test(priority = 8)
	public void Available() throws IOException {
		for (WebElement element : Hamburgersidepage.listelements) {
			if (element.getText().equals("Available")) {
				System.out.println("Available Items");
				element.click();
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				driver.findElement(By.xpath("/html/body/p-dynamicdialog/div/div")).isDisplayed();
				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
				String ExpectedHeader = "Available Items";
				Assert.assertEquals(ExpectedHeader, Hamburgersidepage.Tableheadername.getText());
				Hamburgersidepage.Closebutton.click();
				break;
			}
		}
	}

	@Test(priority = 9)
	public void FloatIn() throws IOException {
		for (WebElement element : Hamburgersidepage.listelements) {
			if (element.getText().equals("Float In")) {
				System.out.println("Float In");
				element.click();
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				driver.findElement(By.xpath("/html/body/p-dynamicdialog/div/div")).isDisplayed();
				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
				String ExpectedHeader = "Float In";
				Assert.assertEquals(ExpectedHeader, Hamburgersidepage.Tableheadername.getText());
				Hamburgersidepage.Closebutton.click();
				break;
			}
		}
	}

	@Test(priority = 10)
	public void FloatOut() throws IOException {
		for (WebElement element : Hamburgersidepage.listelements) {
			if (element.getText().equals("Float Out")) {
				System.out.println("Float Out");
				element.click();
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				driver.findElement(By.xpath("/html/body/p-dynamicdialog/div/div")).isDisplayed();
				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
				String ExpectedHeader = "Float Out";
				Assert.assertEquals(ExpectedHeader, Hamburgersidepage.Tableheadername.getText());
				Hamburgersidepage.Closebutton.click();
				break;
			}
		}
	}

	@Test(priority=11)
	public void Reprint() throws IOException {	
	for(WebElement element : Hamburgersidepage.listelements){
	   if (element.getText().equals("Reprint")){
	    	System.out.println("Reprint");
	    	element.click();
	    	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	        driver.findElement(By.xpath("/html/body/p-dynamicdialog/div/div")).isDisplayed();
	        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	      	String ExpectedHeader ="Reprint";
	    	Assert.assertEquals(ExpectedHeader, Hamburgersidepage.Tableheadername.getText());
	    	//Hamburgersidepage.Closebutton.click();
	    	break;
		}		    	
		}
		
	}
}